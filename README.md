# Mike Jang's Qualifications

Mike Jang's qualifications for the Observe Principal Technical Writer position.

- Resume: mjangResume_observe.docx
- Writing Samples: listOfWritingSamples.md
- Cover Letter: coverLetter_mjang_observe.docx
- Speaking Experience: speakingExperience.md

